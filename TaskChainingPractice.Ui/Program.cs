﻿using System;
using System.Threading.Tasks;
using TaskChainingPractice.Domain;

namespace TaskChainingPractice.Ui
{
    internal static class Program
    {
        static async Task Main()
        {
            var practiceTasks = new PracticeTasks();

            var average = await Task.Run(practiceTasks.CreateArrayOfTenRandomIntegers)
                                    .ContinueWith(initialArray => practiceTasks.MultiplyArrayElementsByRandomValue(initialArray.Result, out int multiplier))
                                    .ContinueWith(multipliedArray => practiceTasks.SortArrayByAscending(multipliedArray.Result))
                                    .ContinueWith(sortedAndMultipliedArray => practiceTasks.GetAverageValue(sortedAndMultipliedArray.Result));

            Console.WriteLine($"\n---\nAverage after finishing all tasks = {average}");

            Console.ReadLine();
        }
    }
}
