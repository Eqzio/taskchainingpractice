﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TaskChainingPractice.Domain;

namespace TaskChainingPractice.Tests
{
    [TestFixture]
    public class PracticeTasksTests
    {
        private PracticeTasks practiceTasks;

        [SetUp]
        public void SetUp()
        {
            practiceTasks = new PracticeTasks();
        }

        [Test]
        public void CreateArrayOfTenRandomIntegers_ReturnsArrayWithLengthEqualToTen()
        {
            var expectedLength = 10;
            var actualLength = practiceTasks.CreateArrayOfTenRandomIntegers().Length;

            Assert.AreEqual(expectedLength, actualLength);
        }

        [Test]
        public void CreateArrayOfTenRandomIntegers_ProperlyPopulatedByRandomNumbers()
        {
            var randomIntegersArray = practiceTasks.CreateArrayOfTenRandomIntegers();
            bool hasDiverseValues = randomIntegersArray.Any(number => number != default);

            Assert.IsTrue(hasDiverseValues, "Values weren't populated properly or test was really unlucky");
        }

        [Test]
        public void MultiplyArrayElementsByRandomValue_ArrayIsNull_ThrowsArgumentNullException()
        {
            var ex = Assert.Throws<ArgumentNullException>(() => practiceTasks.MultiplyArrayElementsByRandomValue(null, out int multiplier));
            Assert.That(ex.ParamName, Is.EqualTo("array"));
        }

        [TestCaseSource(nameof(ArrayWithOutOfRangeElements))]
        public void MultiplyArrayElementsByRandomValue_AtleastOneArrayElementHasInvalidValue_ThrowsArgumentOutOfRangeException(int[] array)
        {
            var ex = Assert.Throws<ArgumentOutOfRangeException>(() => practiceTasks.MultiplyArrayElementsByRandomValue(array, out int multiplier));
            Assert.That(ex.ParamName, Is.EqualTo("array"));
        }

        [TestCaseSource(nameof(ArraysWithValidInput))]
        public void MultiplyArrayElementsByRandomValue_ValidInput_CorrectlyMultipliesValues(int[] originalArray)
        {
            var actualArray = practiceTasks.MultiplyArrayElementsByRandomValue(originalArray, out int multiplier);
            var expectedArray = originalArray.Select(number => number * multiplier).ToArray();

            Assert.AreEqual(expectedArray, actualArray);
        }

        [Test]
        public void SortArrayByAscending_ArrayIsNull_ThrowsArgumentNullException()
        {
            var ex = Assert.Throws<ArgumentNullException>(() => practiceTasks.SortArrayByAscending(null));
            Assert.That(ex.ParamName, Is.EqualTo("array"));
        }


        [TestCaseSource(nameof(ArraysWithValidInput))]
        public void SortArrayByAscending_ValidInput_ReturnsSortedArray(int[] array)
        {
            var expected = array;
            Array.Sort(expected);

            var actual = practiceTasks.SortArrayByAscending(array);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetAverageValue_ArrayIsNull_ThrowsArgumentNullException()
        {
            var ex = Assert.Throws<ArgumentNullException>(() => practiceTasks.GetAverageValue(null));
            Assert.That(ex.ParamName, Is.EqualTo("array"));
        }

        [Test]
        public void GetAverageValue_ArrayIsEmpty_ThrowsArgumentException()
        {
            var array = new int[0];
            var ex = Assert.Throws<ArgumentException>(() => practiceTasks.GetAverageValue(array));
            Assert.That(ex.ParamName, Is.EqualTo("array"));
        }

        [TestCaseSource(nameof(ArraysWithValidInput))]
        public void GetAverageValue_ValidInput_CorrectlyReturnsAverageValue(int[] array)
        {
            var expected = array.Average();
            var actual = practiceTasks.GetAverageValue(array);

            Assert.AreEqual(expected, actual);
        }

        private static IEnumerable<int[]> ArrayWithOutOfRangeElements()
        {
            yield return new int[] { PracticeTasks.MinRandomValue - 1 };
            yield return new int[] { PracticeTasks.MaxRandomValue + 1 };
        }

        private static IEnumerable<int[]> ArraysWithValidInput()
        {
            yield return new int[] { 100_000};
            yield return new int[] { 0, 1, 2, 3, 4, 5 };
            yield return new int[] { PracticeTasks.MinRandomValue, 50, PracticeTasks.MaxRandomValue, 0 };
        }
    }
}
